import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CategorydetailsComponent } from './categorydetails/categorydetails.component';
import { CategoryButtonComponent } from './category-button/category-button.component';
import { TableComponetComponent } from './table-componet/table-componet.component';
import {CatetgoryServiceService} from "./service/catetgory-service.service";
import {ConfigServiceService} from "./service/config-service.service";
import {HttpModule} from "@angular/http";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    CategorydetailsComponent,
    CategoryButtonComponent,
    TableComponetComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [
    CatetgoryServiceService,
    ConfigServiceService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
