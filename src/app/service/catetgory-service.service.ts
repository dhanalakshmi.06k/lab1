import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigServiceService} from "./config-service.service";

@Injectable({
  providedIn: 'root'
})

export class CatetgoryServiceService {

  getAllCategories() {
    return this.http.get(this.configService.appConfig.appBaseUrl + '/branch/getBranchDetails' );
  }



  constructor(private http: HttpClient, public configService: ConfigServiceService ) { }
}
