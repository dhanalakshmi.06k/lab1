import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigServiceService {

  public appConfig = {
    appBaseUrl: 'http://localhost:1234'
  };
  constructor() { }
}
